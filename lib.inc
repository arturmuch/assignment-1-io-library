%define SYS_EXIT 60
%define SPACE 0x20
%define HORIZONTAL_TAB 0x9
%define SUBSTITUTION 0xA
section .text
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall
    ret
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .strl_ret 
        inc rax
        jmp .loop
    .strl_ret:
        ret 

print_string:
    push rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    pop rsi
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi    
    mov rsi, rsp 
    mov rax, 1  
    mov rdx, 1  
    mov rdi,  1   
    syscall
    pop rdi 
    ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
    

print_uint:
    mov r10, rsp            ; Вершина стека
    mov rsi, 10             ; Делитель
    mov rax, rdi            ; Адресс строки
    push 0                  ; нуль терминатор
    .loop:
        mov rdx, 0              ; для остатка
        div rsi                 ; результат в rax, остаток в rdx
        add rdx, '0'            ; перевод в аски
        mov dh, byte[rsp]       ; считываем веришну стека
        inc rsp          
        push dx                 
        test rax, rax
        jnz .loop               
    mov rdi, rsp            
    push r10                
    call print_string       
    pop rsp                 
    ret

print_int:
    test rdi, rdi
    jge .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi ;тк отрицательное
    .print:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.loop:
    mov al, byte[rdi]
    mov dl, byte[rsi]
    cmp al, dl
    jne .not_equal
    cmp al, 0
    je .done
    inc rdi
    inc rsi
    jmp .loop
.not_equal:
    mov rax, 0
    ret
.done:
    mov rax, 1
    ret

read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    mov rdi, 0
    syscall
    pop rax
    ret

read_word:
    push r12
    push r13
    push r14
    mov rax, 0
    mov r9, rdi
    .loop:
        mov r12, r9
        mov r13, rdi
        mov r14, rsi
        call read_char
        mov rsi, r14
        mov rdi, r13
        mov r9, r12
        cmp rax, SPACE
        je .loop
        cmp rax, HORIZONTAL_TAB
        je .loop 
        cmp rax, SUBSTITUTION
    .read:
        cmp rax, 0
        je .endofs
        cmp rax, SPACE
        je .endofs
        cmp rax, HORIZONTAL_TAB
        je .endofs
        cmp rax, SUBSTITUTION
        je .endofs

        dec rsi
        cmp rsi, 0
        je .errorr
        mov byte[rdi], al
        inc rdi

        mov r12, r9
        mov r13, rdi
        mov r14, rsi
        call read_char
        mov rsi, r14
        mov rdi, r13
        mov r9, r12
        jmp .read
    .endofs:
        mov byte[rdi], 0
        mov rax, r9
        sub rdi, r9
        mov rdx, rdi
        pop r14
        pop r13
        pop r12
        ret
    .errorr:
        mov rax, 0
        pop r14
        pop r13
        pop r12
        ret
    
parse_uint:
    xor rax, rax
    xor r10, r10 ;length
    .loop:
        mov r9, rax
        xor rax, rax
        mov al, byte[rdi + r10]
        test rax, rax
        jz .finish
        mov rsi, rax 
        mov rax, r9 
        sub sil, 48
        cmp sil, 0
        js .empty
        cmp sil, 10
        jns .finish
        mov rcx, 10
        mul rcx
        add rax, rsi
        inc r10
        ;inc rdi
        jmp .loop
    .finish:
        mov rax, r9
        mov rdx, r10
        ret
    .empty:
        xor rdx, rdx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    ;xor rax, rax
    cmp byte[rdi], "-"     
    jne parse_uint
    inc  rdi
    call parse_int
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov rax, 0            
    mov r10, 0            

.copy_loop:
    mov al, byte[rdi + r10]      
    test al, al            
    jz .copy_done            

    cmp r10, rdx            
    jae .copy_error         

    mov [rsi + r10], al           
    inc rax                 
    inc r10                 
    jmp .copy_loop          

.copy_error:
    mov rax, 0            
    ret

.copy_done:
    mov byte[rsi + r10], 0 
    ret
